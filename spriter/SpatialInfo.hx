package spriter;

import spriter.internal.Util;

class SpatialInfo
{
    public var x : Float;
    public var y : Float;
    public var angle : Float;
    public var scaleX : Float;
    public var scaleY : Float;
    public var a : Float;
    public var spin : Int;

    public function new()
    {
        x = 0;
        y = 0;
        angle = 0;
        scaleX = 1;
        scaleY = 1;
        a = 1;
        spin = 1;
    }

    public function lerp(aa : SpatialInfo, bb : SpatialInfo,
                         _spin : Int, tt : Float) : Void
    {
        x = Util.lerp(aa.x, bb.x, tt);
        y = Util.lerp(aa.y, bb.y, tt);
        angle = Util.angleLerp(aa.angle, bb.angle, _spin, tt);
        scaleX = Util.lerp(aa.scaleX, bb.scaleX, tt);
        scaleY = Util.lerp(aa.scaleY, bb.scaleY, tt);
        a = Util.lerp(aa.a, bb.a, tt);
        spin = _spin;
    }

    public function unmapFromParent(parent : SpatialInfo) : Void
    {
        angle += parent.angle;
        scaleX *= parent.scaleX;
        scaleY *= parent.scaleY;
        a *= parent.a;

        if(x != 0 || y != 0)
        {
            var preMultX = x * parent.scaleX;
            var preMultY = y * parent.scaleY;
            var parentRad = Util.toRadians(parent.angle);
            var sin = Math.sin(parentRad);
            var cos = Math.cos(parentRad);
            x = parent.x + (preMultX * cos) - (preMultY * sin);
            y = parent.y + (preMultX * sin) + (preMultY * cos);
        }
        else 
        {
            x += parent.x;
            y += parent.y;
        }
    }
}
