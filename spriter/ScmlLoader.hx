package spriter;

import haxe.xml.Fast;

import spriter.internal.*;
import spriter.internal.Animation;
import spriter.internal.Timeline;

class ScmlLoader
{
    private var mImageLoader : IImageLoader;
    
    public function new(imageLoader : IImageLoader)
    {
        mImageLoader = imageLoader;
    }
    
    public function xml(data : Xml)
    {
        var xml = new Fast(data);

        if(xml.name != "spriter_data" ||
           !xml.has.scml_version ||
           xml.att.scml_version != "1.0")
        {
            throw "unsupported format";
        }

        var scml = new ScmlObject();
        var scmlFriend : ScmlObjectFriend = scml;
        for(el in xml.elements)
        {
            if(el.name == "folder")
            {
                scml.folders.push(parseFolder(el));
            }
            else if(el.name == "entity")
            {
                scmlFriend.mEntities.push(parseEntity(el));
            }
        }

        return scml;
    }

    private function parseFolder(node : Fast) : Folder
    {
        var folder = new Folder();
        folder.name = node.att.name;
        for(child in node.nodes.file)
        {
            var file = new File();
            file.name = child.att.name;
            file.pivotX = Std.parseFloat(child.att.pivot_x);
            file.pivotY = Std.parseFloat(child.att.pivot_y);
            file.fileRef = mImageLoader.loadImage(file.name);

            folder.files.push(file);
        }

        return folder;
    }

    private function parseEntity(node : Fast) : Entity
    {
        var entity = new Entity();
        entity.name = node.att.name;

        for(animation in node.nodes.animation)
        {
            entity.animations.push(parseAnimation(animation));
        }

        return entity;
    }

    private function parseAnimation(node : Fast) : Animation
    {
        var animation = new Animation();
        animation.name = node.att.name;
        animation.length = Std.parseInt(node.att.length);

        if(node.has.looping && node.att.looping == "false")
        {
            animation.loopType = LoopType.Single;
        }

        if(node.hasNode.mainline)
        {
            for(keyNode in node.node.mainline.nodes.key)
            {
                var key = new MainlineKey();
                if(keyNode.has.time)
                {
                    key.time = Std.parseInt(keyNode.att.time);
                }

                for(el in keyNode.elements)
                {
                    var ref = new Ref();
                    if(el.has.parent)
                    {
                        ref.parent = Std.parseInt(el.att.parent);
                    }
                    ref.timeline = Std.parseInt(el.att.timeline);
                    ref.key = Std.parseInt(el.att.key);

                    if(el.name == "bone_ref")
                    {
                        key.boneRefs.push(ref);
                    }
                    else if(el.name == "object_ref")
                    {
                        key.objectRefs.push(ref);
                    }
                }

                animation.mainlineKeys.push(key);
            }
        }

        for(timelineNode in node.nodes.timeline)
        {
            animation.timelines.push(parseTimeline(timelineNode));
        }

        return animation;
    }

    private function parseTimeline(node : Fast) : Timeline
    {
        var timeline = new Timeline();
        timeline.name = node.att.name;

        if(node.has.object_type)
        {
            switch(node.att.object_type)
            {
            case "sprite":
                timeline.objectType = ObjectType.Sprite;
            case "bone":
                timeline.objectType = ObjectType.Bone;
            case "box":
                timeline.objectType = ObjectType.Box;
            case "point":
                timeline.objectType = ObjectType.Point;
            case "sound":
                timeline.objectType = ObjectType.Sound;
            case "entity":
                timeline.objectType = ObjectType.Entity;
            case "variable":
                timeline.objectType = ObjectType.Variable;
            default:
            }
        }

        for(keyNode in node.nodes.key)
        {
            var key : SpatialTimelineKey;
            var infoNode : Fast;

            if(keyNode.hasNode.bone)
            {
                key = new BoneTimelineKey();
                infoNode = keyNode.node.bone;
            }
            else if(keyNode.hasNode.object)
            {
                infoNode = keyNode.node.object;

                var spriteKey = new SpriteTimelineKey();
                spriteKey.folder = Std.parseInt(infoNode.att.folder);
                spriteKey.file = Std.parseInt(infoNode.att.file);

                if(infoNode.has.pivot_x ||
                   infoNode.has.pivot_y)
                {
                    spriteKey.useDefaultPivot = false;

                    if(infoNode.has.pivot_x)
                    {
                        spriteKey.pivotX = Std.parseFloat(infoNode.att.pivot_x);
                    }
                    if(infoNode.has.pivot_y)
                    {
                        spriteKey.pivotY = Std.parseFloat(infoNode.att.pivot_y);
                    }
                }
                
                key = spriteKey;
            }
            else
            {
                continue;
            }

            if(keyNode.has.time)
            {
                key.time = Std.parseInt(keyNode.att.time);
            }

            // TODO curve_type

            if(keyNode.has.c1)
            {
                key.c1 = Std.parseFloat(keyNode.att.c1);
            }
            if(keyNode.has.c2)
            {
                key.c2 = Std.parseFloat(keyNode.att.c2);
            }

            if(keyNode.has.spin)
            {
                key.info.spin = Std.parseInt(keyNode.att.spin);
            }
            if(infoNode.has.x)
            {
                key.info.x = Std.parseFloat(infoNode.att.x);
            }
            if(infoNode.has.y)
            {
                key.info.y = Std.parseFloat(infoNode.att.y);
            }
            if(infoNode.has.angle)
            {
                key.info.angle = Std.parseFloat(infoNode.att.angle);
            }
            if(infoNode.has.scale_x)
            {
                key.info.scaleX = Std.parseFloat(infoNode.att.scale_x);
            }
            if(infoNode.has.scale_y)
            {
                key.info.scaleY = Std.parseFloat(infoNode.att.scale_y);
            }
            if(infoNode.has.a)
            {
                key.info.a = Std.parseFloat(infoNode.att.a);
            }

            timeline.keys.push(key);
        }
        
        return timeline;
    }
}

private typedef ScmlObjectFriend =
{
    private var mEntities : Array<Entity>;
}
