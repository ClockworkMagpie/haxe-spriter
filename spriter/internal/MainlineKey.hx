package spriter.internal;

class MainlineKey
{
    public var time : Int;
    public var boneRefs : Array<Ref>;
    public var objectRefs : Array<Ref>;

    public function new()
    {
        time = 0;
        boneRefs = [];
        objectRefs = [];
    }
}
