package spriter.internal;

enum CurveType
{
    Instant;
    Linear;
    Quadratic;
    Cubic;
}

class TimelineKey
{
    public var time : Int;
    public var curveType : CurveType;
    public var c1 : Float;
    public var c2 : Float;

    public function new()
    {
        time = 0;
        curveType = CurveType.Linear;
    }

    public function observe(object : ScmlObject,
                            observer : ITimelineVisitor) : Void
    {
    }

    public function copyFrom(other : TimelineKey)
    {
        time = other.time;
        curveType = other.curveType;
        c1 = other.c1;
        c2 = other.c2;
    }

    inline public function interpolate(keyA : TimelineKey,
                                       keyB : TimelineKey,
                                       nextKeyTime : Int,
                                       currentTime : Int) : Void
    {
        time = keyA.time;
        curveType = keyA.curveType;
        c1 = keyA.c1;
        c2 = keyA.c2;
        
        linear(keyA, keyB, keyA.interpolateTime(keyB, nextKeyTime, currentTime));
    }

    private function interpolateTime(nextKey : TimelineKey,
                                     nextKeyTime : Int,
                                     currentTime : Int) : Float
    {
        var dt = (currentTime - time) / (nextKeyTime - time);

        switch(curveType)
        {
        case Instant:
            return 0;
        case Linear:
            return dt;
        case Quadratic:
            return Util.quadratic(0, c1, 0, dt);
        case Cubic:
            return Util.cubic(0, c1, c2, 1, dt);
        }
    }

    private function linear(keyA : TimelineKey,
                            keyB : TimelineKey,
                            time : Float) : Void
    {
    }
}
