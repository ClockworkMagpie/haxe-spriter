package spriter.internal;

enum LoopType
{
    Looping;
    Single;
}

class Animation
{
    public var name : String;
    public var length : Int;
    public var loopType : LoopType;
    public var mainlineKeys : Array<MainlineKey>;
    public var timelines : Array<Timeline>;

    public function new()
    {
        mainlineKeys = [];
        timelines = [];
        loopType = LoopType.Looping;
    }

    public function setTime(object : ScmlObject,
                            timeMs : Int, observer : ITimelineVisitor)
    {
        switch(loopType)
        {
        case Looping:
            timeMs = timeMs % length;
        case Single:
            timeMs = Std.int(Math.min(timeMs, length));
        }

        updateCharacter(object, mainlineKeyFromTime(timeMs),
                        timeMs, observer);
    }

    private function mainlineKeyFromTime(time : Int) : MainlineKey
    {
        var ii = 0;
        while(ii < mainlineKeys.length-1)
        {
            if(mainlineKeys[ii + 1].time > time)
            {
                break;
            }
            ++ii;
        }

        return mainlineKeys[ii];
    }

    private function updateCharacter(object : ScmlObject,
                                     key : MainlineKey, time : Int,
                                     observer : ITimelineVisitor)
    {
        var transformedBoneKeys = new Array<BoneTimelineKey>();
        for(ref in key.boneRefs)
        {
            var parentInfo : SpatialInfo;
            if(ref.parent >= 0)
            {
                parentInfo = transformedBoneKeys[ref.parent].info;
            }
            else
            {
                parentInfo = observer.characterInfo;
            }

            var currentKey = new BoneTimelineKey();
            keyFromRef(ref, time, currentKey);
            currentKey.info.unmapFromParent(parentInfo);
            transformedBoneKeys.push(currentKey);
        }

        var objectKeys = new Array<SpriteTimelineKey>();
        for(ref in key.objectRefs)
        {
            var parentInfo : SpatialInfo;
            if(ref.parent >= 0)
            {
                parentInfo = transformedBoneKeys[ref.parent].info;
            }
            else
            {
                parentInfo = observer.characterInfo;
            }

            var currentKey = new SpriteTimelineKey();
            keyFromRef(ref, time, currentKey);
            currentKey.info.unmapFromParent(parentInfo);
            objectKeys.push(currentKey);
        }

        for(key in objectKeys)
        {
            key.observe(object, observer);
        }
    }

    private function keyFromRef(ref : Ref, time : Int,
                                key : TimelineKey) : Void
    {
        var timeline = timelines[ref.timeline];
        var keyA = timeline.keys[ref.key];

        if(timeline.keys.length == 1)
        {
            key.copyFrom(keyA);
            return;
        }

        var nextKeyIdx = ref.key + 1;
        if(nextKeyIdx >= timeline.keys.length)
        {
            if(loopType == Looping)
            {
                nextKeyIdx = 0;
            }
            else
            {
                key.copyFrom(keyA);
                return;
            }
        }

        var keyB = timeline.keys[nextKeyIdx];
        var keyBTime = keyB.time;
        if(keyBTime < keyA.time)
        {
            keyBTime += length;
        }

        key.interpolate(keyA, keyB, keyBTime, time);
    }
}
