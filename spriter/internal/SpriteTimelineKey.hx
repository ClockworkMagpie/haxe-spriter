package spriter.internal;

class SpriteTimelineKey extends SpatialTimelineKey
{
    public var folder : Int;
    public var file : Int;
    public var pivotX : Float;
    public var pivotY : Float;
    public var useDefaultPivot : Bool;

    public function new()
    {
        super();
        
        pivotX = 0;
        pivotY = 1;
        useDefaultPivot = true;
    }

    override public function copyFrom(other : TimelineKey)
    {
        super.copyFrom(other);

        var spriteOther : SpriteTimelineKey = cast other;
        folder = spriteOther.folder;
        file = spriteOther.file;
        pivotX = spriteOther.pivotX;
        pivotY = spriteOther.pivotY;
        useDefaultPivot = spriteOther.useDefaultPivot;
    }

    override private function linear(keyA : TimelineKey,
                                     keyB : TimelineKey,
                                     time : Float) : Void
    {
        super.linear(keyA, keyB, time);

        var spA : SpriteTimelineKey = cast keyA;
        var spB : SpriteTimelineKey = cast keyB;

        folder = spA.folder;
        file = spA.file;
        useDefaultPivot = spA.useDefaultPivot;

        if(!useDefaultPivot)
        {
            pivotX = Util.lerp(spA.pivotX, spB.pivotX, time);
            pivotY = Util.lerp(spA.pivotY, spB.pivotY, time);
        }
    }

    override public function observe(object : ScmlObject,
                                     observer : ITimelineVisitor) : Void
    {
        var file = object.folders[folder].files[file];
        
        var paintPivotX : Float;
        var paintPivotY : Float;
        if(useDefaultPivot)
        {
            paintPivotX = file.pivotX;
            paintPivotY = file.pivotY;
        }
        else
        {
            paintPivotX = pivotX;
            paintPivotY = pivotY;
        }

        observer.sprite(file.fileRef, info.x, info.y,
                        info.scaleX, info.scaleY,
                        Util.toRadians(info.angle),
                        paintPivotX, paintPivotY);
    }
}
