package spriter.internal;

class Folder
{
    public var name : String;
    public var files : Array<File>;

    public function new()
    {
        files = [];
    }
}
