package spriter.internal;

class Util
{
    inline static public function toRadians(deg : Float) : Float
    {
        return deg * Math.PI / 180;
    }
    
    inline static public function lerp(aa : Float, bb : Float,
                                        tt : Float) : Float
    {
        return ((bb - aa) * tt) + aa;
    }

    static public function angleLerp(aa : Float, bb : Float,
                                      spin : Int, tt : Float) : Float
    {
        if(spin == 0)
        {
            return aa;
        }

        if(spin > 0)
        {
            if(bb - aa < 0)
            {
                bb += 360;
            }
        }
        else if(spin < 0)
        {
            if(bb - aa > 0)
            {
                bb -= 360;
            }
        }

        return lerp(aa, bb, tt);
    }

    static public function quadratic(aa : Float, bb: Float,
                                      cc : Float, tt : Float) : Float
    {
        return lerp(lerp(aa, bb, tt), lerp(bb, cc, tt), tt);
    }

    static public function cubic(aa : Float, bb: Float,
                                  cc : Float, dd : Float,
                                  tt : Float) : Float
    {
        return lerp(quadratic(aa, bb, cc, tt),
                    quadratic(bb, cc, dd, tt),
                    tt);
    }
}
