package spriter.internal;

class SpatialTimelineKey extends TimelineKey
{
    public var info : SpatialInfo;

    public function new()
    {
        super();
        
        info = new SpatialInfo();
    }

    override public function copyFrom(other : TimelineKey)
    {
        super.copyFrom(other);

        var spatialOther : SpatialTimelineKey = cast other;
        info.x = spatialOther.info.x;
        info.y = spatialOther.info.y;
        info.angle = spatialOther.info.angle;
        info.scaleX = spatialOther.info.scaleX;
        info.scaleY = spatialOther.info.scaleY;
        info.a = spatialOther.info.a;
        info.spin = spatialOther.info.spin;
    }

    override private function linear(keyA : TimelineKey,
                                     keyB : TimelineKey,
                                     time : Float) : Void
    {
        var spA : SpatialTimelineKey = cast keyA;
        var spB : SpatialTimelineKey = cast keyB;
        info.lerp(spA.info, spB.info, spA.info.spin, time);
    }
}
