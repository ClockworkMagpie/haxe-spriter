package spriter.internal;

enum ObjectType
{
    Sprite;
    Bone;
    Box;
    Point;
    Sound;
    Entity;
    Variable;
}

class Timeline
{
    public var name : String;
    public var objectType : ObjectType;
    public var keys : Array<TimelineKey>;

    public function new()
    {
        keys = [];
        objectType = ObjectType.Sprite;
    }
}
