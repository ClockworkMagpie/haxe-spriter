package spriter.internal;

class File
{
    public var name : String;
    public var pivotX : Float;
    public var pivotY : Float;

    // TODO template parameter?
    public var fileRef : Dynamic;

    public function new()
    {
        pivotX = 0;
        pivotY = 1;
    }
}
