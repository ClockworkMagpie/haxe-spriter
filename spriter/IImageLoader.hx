package spriter;

interface IImageLoader
{
    function loadImage(relativePath : String) : Dynamic;
}
