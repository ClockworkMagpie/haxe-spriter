package spriter;

interface ITimelineVisitor
{
    var characterInfo : SpatialInfo;

    function sprite(fileRef : Dynamic,
                    cx : Float, cy : Float,
                    scaleX : Float, scaleY : Float,
                    angle : Float,
                    offsetX : Float, offsetY : Float) : Void;
}
