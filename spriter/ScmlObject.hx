package spriter;

import spriter.internal.*;

class ScmlObject
{
    private var mEntities : Array<Entity>;
    // TODO character maps

    public var folders(default, null) : Array<Folder>;
    public var currentEntity : Int;
    public var currentAnimation : Int;

    public var entity(get, never) : Entity;

    private function get_entity()
    {
        return mEntities[currentEntity];
    }

    public function new()
    {
        folders = [];
        mEntities = [];
    }

    inline public function observeTime(timeMs : Int,
                                       observer : ITimelineVisitor)
    {
        mEntities[currentEntity].animations[currentAnimation]
            .setTime(this, timeMs, observer);
    }

    public function debug()
    {
        for(folder in folders)
        {
            for(file in folder.files)
            {
                trace("folder", folder.name, file.name, file.pivotX, file.pivotY);
            }
        }

        for(entity in mEntities)
        {
            for(animation in entity.animations)
            {
                trace(animation.name, animation.length, animation.loopType);
            }
        }
    }
}
